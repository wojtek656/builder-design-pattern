import java.math.BigDecimal;

public class Main {

	public static void main(String[] args) {

		Product prod1 = new Product.Builder(1, "iPhone 5s", new BigDecimal(500)).category("Smart Phones")
				.isDiscounted(false).manufacturer("Apple").build();
	}
}
