import java.math.BigDecimal;

public class Product {

	private int id;
	private String name;
	private BigDecimal unitPrice;
	private String category;
	private String manufacturer;
	private int unitInStock;
	private int unitBought;
	private boolean isDiscounted;
	private String supplier;

	public static class Builder {

		private int id;
		private String name;
		private BigDecimal unitPrice;

		private String category = "";
		private String manufacturer ="";
		private int unitInStock = 0;
		private int unitBought = 0;
		private boolean isDiscounted = false;
		private String supplier = "";

		public Builder(int id, String name, BigDecimal unitPrice) {

			this.id = id;
			this.name = name;
			this.unitPrice = unitPrice;
		}

		public Builder category(String val) {
			category = val;
			return this;
		}

		public Builder manufacturer(String val) {
			manufacturer = val;
			return this;
		}

		public Builder unitInStock(int val) {
			unitInStock = val;
			return this;
		}

		public Builder unitBought(int val) {
			unitBought = val;
			return this;
		}

		public Builder isDiscounted(boolean val) {
			isDiscounted = val;
			return this;
		}

		public Builder supplier(String val) {
			supplier = val;
			return this;
		}

		public Product build() {

			return new Product(this);
		}

	}

	private Product(Builder builder) {

		id = builder.id;
		name = builder.name;
		unitPrice = builder.unitPrice;
		category = builder.category;
		manufacturer = builder.manufacturer;
		unitInStock = builder.unitInStock;
		unitBought = builder.unitBought;
		isDiscounted = builder.isDiscounted;
		supplier = builder.supplier;
	}

}
